/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dacodes.prueba2;

/**
 *
 * @author karimy
 */
public final class MatrizEspiral {

    //Método para recorrer la matriz en espiral y ver la dirección del movimiento
    public static String getDireccionFinal(int n, int m) {
        //Declaración de variables
        int[][] matriz = new int[n][m]; //Matriz especificada en la entrada
        String direccion = ""; //Dirección del recorrido actual
        int maxFila = n - 1; //Numero filas máximo
        int maxColumna = m - 1; //Numero columnas máximo
        int filaAuxiliar = 0; //Contador auxiliar para filas
        int columnaAuxiliar = 0; //Contador auxiliar para las columnas
        int band = 0;  //Dirección del recorrido
        int elementosRecorridos = 0; //Elementos recorridos
        int elementos = matriz.length * matriz[0].length; //Tamaño de la matriz [ nxm ]

        // Recorrido en espiral
        while (elementosRecorridos < elementos) {
            switch (band) {
                case 0: // Recorrido de izquierda a derecha
                    for (int x = filaAuxiliar; x <= maxColumna; x++) {
                        //System.out.println("matriz [" + filaAuxiliar + "][" + x + "]" + matriz[filaAuxiliar][x]);
                        if (elementosRecorridos == elementos - 1) {
                            direccion = "R";
                        }
                        elementosRecorridos++;

                    }
                    band++; //Cambiar la dirección del recorrido
                    break;

                case 1: //Recorrido de arriba abajo
                    for (int x = filaAuxiliar + 1; x <= maxFila; x++) {
                        //System.out.println("matriz [" + x + "][" + maxColumna + "]" + matriz[x][maxColumna]);
                        if (elementosRecorridos == elementos - 1) {
                            direccion = "D";
                        }
                        elementosRecorridos++;
                    }
                    band++;
                    break;

                case 2: //Recorrido de derecha a izquierda
                    for (int x = maxFila - 1; x >= columnaAuxiliar; x--) {
                        //System.out.println("matriz [" + maxFila + "][" + x + "]" + matriz[maxFila][x]);
                        if (elementosRecorridos == elementos - 1) {
                            direccion = "L";
                        }
                        elementosRecorridos++;
                    }
                    band++;
                    break;

                case 3: //Recorrido de abajo a arriba
                    for (int x = maxFila - 1; x >= filaAuxiliar + 1; x--) {
                        //System.out.println("matriz [" + x + "][" + columnaAuxiliar + "]" + matriz[x][columnaAuxiliar]);

                        if (elementosRecorridos == elementos - 1) {
                            direccion = "U";
                        }
                        elementosRecorridos++;
                    }
                    band = 0;
                    //Siguiente nivel en la espiral
                    filaAuxiliar++;
                    columnaAuxiliar++;
                    maxColumna --;
                    maxFila --;
                    break;
            }

        }

        return direccion;
    }

    public static void main(String[] args) {
        String direccion = MatrizEspiral.getDireccionFinal(3, 3);

        System.out.println(direccion);
    }

}
