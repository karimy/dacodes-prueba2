/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dacodes.prueba2;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * @author karimy
 */
public class DacodesPrueba2 {

    // Declaración de variables
    HashMap<String, DatosMatriz> listaMatrices = new HashMap<String, DatosMatriz>();
    int numeroPruebas = 0;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DacodesPrueba2 prueba2 = new DacodesPrueba2();
        prueba2.solicitarDatosEntrada();
        prueba2.mostrarDatosSalida();

    }

    //Método para solicitar los datos de las matrices
    public void solicitarDatosEntrada() {
        Scanner entradaEscaner = new Scanner(System.in); //Para lectura desde la consola

        //Validación de datos de prueba de entrada entrada
        while (numeroPruebas <= 0 || numeroPruebas >= 5000) {
            System.out.println("Introduzca el número de pruebas (T) a realizar:");
            numeroPruebas = entradaEscaner.nextInt();
            System.out.println("Número de pruebas a realizarse: " + numeroPruebas);
        }

        for (int i = 1; i <= numeroPruebas; i++) {
            DatosMatriz matrizN = new DatosMatriz();
            System.out.println("Introduzca valor n para matriz " + i);
            int valorN = entradaEscaner.nextInt();
            matrizN.setTamanioN(valorN);

            System.out.println("Introduzca valor m para matriz " + i);
            int valorM = entradaEscaner.nextInt();
            matrizN.setTamanioM(valorM);

            listaMatrices.put("matriz" + i, matrizN);

        }

    }

    public void mostrarDatosSalida() {
        for (int i = 1; i <= numeroPruebas; i++) {
            String clave = "matriz" + i;
            DatosMatriz matriz = listaMatrices.get(clave);
            int n = matriz.getTamanioN();
            int m = matriz.getTamanioM();

            String direccionFinal = MatrizEspiral.getDireccionFinal(n, m);

            System.out.println("Matriz " + n + "," + m + " = " + direccionFinal);

        }
    }

    // GETTERS Y SETTERS
    public HashMap<String, DatosMatriz> getListaMatrices() {
        return listaMatrices;
    }

    public void setListaMatrices(HashMap<String, DatosMatriz> listaMatrices) {
        this.listaMatrices = listaMatrices;
    }

    public int getNumeroPruebas() {
        return numeroPruebas;
    }

    public void setNumeroPruebas(int numeroPruebas) {
        this.numeroPruebas = numeroPruebas;
    }

}
